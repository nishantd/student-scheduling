#!/usr/bin/python

import optparse
import os, sys
from ortools.constraint_solver import pywrapcp

solver = pywrapcp.Solver('schedule')

num_students = None  # Total number of students.
num_flexblocks = None  # Total number of flexblocks in the week.
num_classes = None  # Total number of classes (i.e. math1, history2, english1, math2,..)
IDEAL_STUDENTS_PER_FLEXBLOCK = None
student_groups_size = None

def _flatten_cube(cube):
  return [cube[i][(j, k)] for i in range(num_classes) for j in range(num_students) for k in range(num_flexblocks)]

def create_class_student_flexblocks_cube():
  # Create the matrices for each class.
  student_classes = []
  for class_id in range(num_classes):
    student_class = {}
    student_classes.append(student_class)
    for student_id in range(num_students):
      for flexblock_id in range(num_flexblocks):
        student_class[(student_id, flexblock_id)] \
            = solver.IntVar(0, 1, 'sc(%i,%i)' % (student_id, flexblock_id))
  return student_classes

def make_same(student_id_1, student_id_2, student_classes):
  for class_id in range(num_classes):
    sc = student_classes[class_id]
    for flexblock_id in range(num_flexblocks):
      solver.Add(sc[(student_id_1, flexblock_id)] == sc[(student_id_2, flexblock_id)])

def create_constraints(student_classes):
  # Constraints:
  # 1. Sum of students in a class_flexblock - class_flexblock_slack <= ideal number of students in a class
  # 2. Student should not be double-booked (with a slack)
  # 3. Each student takes a class exactly once.

  # Constraint 3: Each student takes a class exactly once.
  for class_id in range(num_classes):
    for student_id in range(num_students):
      solver.Add(solver.Sum(student_classes[class_id][(student_id, j)] for j in range(num_flexblocks)) == 1)

  # Number of students per flexblock (per class)
  all_flexblocks = []
  all_flexblocks_slacks = []
  for class_id in range(num_classes):
    flexblocks = []
    flexblocks_slack = []
    for flexblock_id in range(num_flexblocks):
      flexblocks.append(solver.IntVar(0, num_students, 'class_%i_flexblock_%i' % (class_id, flexblock_id,)))
      solver.Add(solver.Sum(student_classes[class_id][(i, flexblock_id)] for i in range(num_students)) == flexblocks[flexblock_id])
      flexblocks_slack.append(solver.IntVar(0, max(IDEAL_STUDENTS_PER_FLEXBLOCK, num_students), 'class_%i_flexblock_%i_slack' % (class_id, flexblock_id)))
      # Constraint 1.
      solver.Add(flexblocks[flexblock_id] - flexblocks_slack[flexblock_id] <= IDEAL_STUDENTS_PER_FLEXBLOCK)
      #solver.Add(flexblocks[flexblock_id] - flexblocks_slack[flexblock_id] == IDEAL_STUDENTS_PER_FLEXBLOCK)
    # This constraint is important for propagation to find there is no solution.
    solver.Add(solver.Sum(flexblocks) == num_students)
    all_flexblocks.extend(flexblocks)
    all_flexblocks_slacks.extend(flexblocks_slack)

  # Students should not be double-booked (with a slack variable)
  student_double_booked_slacks = []
  for student_id in range(num_students):
    for flexblock_id in range(num_flexblocks):
      sl = solver.IntVar(
          0, num_classes, 'student_%i_flexblock_%i_slack' % (student_id, flexblock_id))
      student_double_booked_slacks.append(sl)
      # Constraint 2: Student should not be double booked.
      solver.Add(solver.Sum(student_classes[class_id][(student_id, flexblock_id)] for class_id in range(num_classes)) -1 <= sl)
  return {'student_double_booked_slacks':student_double_booked_slacks,
          'all_flexblocks_slacks':all_flexblocks_slacks,
          'all_flexblocks':all_flexblocks}

def do_search(search_num):
  student_classes = create_class_student_flexblocks_cube()
  #(new_vars, total_slack) = create_constraints(student_classes)
  new_vars_dict = create_constraints(student_classes)

  all_slacks = []
  all_slacks.extend(new_vars_dict['student_double_booked_slacks'])
  all_slacks.extend(new_vars_dict['all_flexblocks_slacks'])
  #print >> sys.stderr, "all_slacks: %s" % (all_slacks,)
  z = solver.Sum(all_slacks)

  #for sv in slack_vars:
  #  objective = solver.Minimize(sv, 1)
  objective = solver.Minimize(z, 1)

  solution = solver.Assignment()
  student_classes_array = _flatten_cube(student_classes)
  solution.Add(student_classes_array)
  solution.Add(new_vars_dict['all_flexblocks'])
  solution.Add(all_slacks)
  solution.Add(z)
  solution.AddObjective(z)

  db = solver.Phase(
      student_classes_array + all_slacks, solver.CHOOSE_MIN_SIZE_LOWEST_MAX, solver.ASSIGN_MIN_VALUE)
  solver.NewSearch(db, [objective])

  #total_slack.SetValue(0)
  for sv in new_vars_dict['student_double_booked_slacks']:
    #sv.SetRange(0,1)
    sv.SetValue(0)
  for flexblock_slacks in new_vars_dict['all_flexblocks_slacks']:
    flexblock_slacks.SetValue(0)
    #flexblock_slacks.SetRange(0,4)
  for flexblock in new_vars_dict['all_flexblocks']:
    flexblock.SetRange(0, IDEAL_STUDENTS_PER_FLEXBLOCK)

  # Create student groups. This *really* is helpful for propagation.
  if student_groups_size and student_groups_size > 1:
    print >> sys.stderr, "making student groups of size %s" % (student_groups_size,)
    for student_id in range(num_students):
      if student_id % student_groups_size != 0:
        make_same((student_id / student_groups_size) * student_groups_size,
                  student_id,
                  student_classes)

  # Try to over-constrain to find the solution. Lessen the constraints if no solution
  # is found.

  if search_num == 0:
    flexblocks_totals = 0
    students_per_flexblock = num_students / num_flexblocks
    if (num_students % num_flexblocks):
      students_per_flexblock += 1
    print >> sys.stderr, "students_per_flexblock: %s" % (students_per_flexblock,)
    small_batch_size = num_students - (students_per_flexblock * (num_flexblocks - 1))
    print >> sys.stderr, "small_batch_size: %s" % (small_batch_size, )
    for flexblock in new_vars_dict['all_flexblocks']:
      flexblock.SetValues([small_batch_size, students_per_flexblock])
      print >> sys.stderr, "set values for %s" % (flexblock,)

  num_solutions = 0
  #print >> sys.stderr, "all_slacks 2: %s" % (all_slacks,)
  while solver.NextSolution():
    assert z.Bound(), "z is not bound?!?!"
    num_solutions += 1
    #print 'SOLUTION: %i, %s' % (num_solutions, new_vars_dict)
    #print 'z: %s' % (z,)
    print "SOLUTION: %i, z: %s" % (num_solutions, z.Max())
    #print "Student double booked slacks:"
    #for sv in new_vars_dict['student_double_booked_slacks']:
    #  if sv.Value() != 0: print sv
    #print "Done Student double booked slacks"
    print "Flexblocks slacks:"
    for fs in new_vars_dict['all_flexblocks_slacks']:
      if fs.Value() != 0: print fs
    print "Done Flexblocks slacks"

    for class_id in range(num_classes):
      print 'CLASS %i:' % (class_id,)
      for student_id in range(num_students):
        print [student_classes[class_id][(student_id, flexblock_id)].Value() for flexblock_id in range(num_flexblocks)]
        print ""
      print "\n\n"
    print "FLEXBLOCKS for CLASS %i" % (class_id,)
    ctr = 0
    for flexblock in new_vars_dict['all_flexblocks']:
      print "flexblock %s: %s" % (ctr, flexblock.Value())
      ctr += 1
    sys.stdout.flush()

    # Check if z is minimized, if it is then break
    if z.Max() == 0:
      print "Minimized z: %s" % (z.Max(),)
      break

  print
  print 'num_solutions:', num_solutions
  print 'failures:', solver.Failures()
  print 'branches:', solver.Branches()
  print 'WallTime:', solver.WallTime(), 'ms'
  return num_solutions


def main():
  global num_students
  global num_flexblocks
  global num_classes
  global IDEAL_STUDENTS_PER_FLEXBLOCK
  global student_groups_size

  parser = optparse.OptionParser()
  parser.add_option('--num-students', type="int", dest="num_students",
                    action="store")
  parser.add_option('--num-flexblocks', type='int', dest='num_flexblocks', action='store')
  parser.add_option('--num-classes', type='int', dest='num_classes', action='store')
  parser.add_option('--students-per-flexblock', type='int', dest='students_per_flexblock',
                    action='store')
  parser.add_option('--student-groups-size', dest='student_groups_size', type='int',
                    action='store', default=None)

  (options, args) = parser.parse_args()

  num_students = options.num_students
  num_flexblocks = options.num_flexblocks
  num_classes = options.num_classes
  IDEAL_STUDENTS_PER_FLEXBLOCK = options.students_per_flexblock
  student_groups_size = options.student_groups_size

  print >> sys.stderr, "num_students=%s num_flexblocks=%s num_classes=%s, IDEAL_STUDENTS_PER_FLEXBLOCK=%s" \
      % (num_students, num_flexblocks, num_classes, IDEAL_STUDENTS_PER_FLEXBLOCK)

  assert num_classes <= num_flexblocks,  \
      "num_classes (%i) cannot be larger than num_flexblocks (%i)" % (num_classes, num_flexblocks)
  for search_num in range(2):
    print >> sys.stderr, "SEARCH_NUM: %s" % (search_num,)
    num_solutions = do_search(search_num)
    print >> sys.stderr, "SEARCH %s returned with num_solutions: %s" % (search_num, num_solutions)
    if num_solutions: break


if __name__ == "__main__":
  main()

# ./cp2.py --num-students=40 --num-flexblocks=10 --num-classes=7 --students-per-flexblock=4
# num_solutions: 1, failures: 9396390, branches: 18792012, WallTime: 272389 ms

# ./cp2.py --num-students=40 --num-flexblocks=10 --num-classes=10 --students-per-flexblock=4
# num_solutions: 1, failures: 9396426, branches: 18792048, WallTime: 450251 ms
# num_solutions: 1, failures: 9395622, branches: 18792048, WallTime: 486347 ms

