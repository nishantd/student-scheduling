#!/usr/bin/python

import os, sys
from ortools.constraint_solver import pywrapcp

solver = pywrapcp.Solver('schedule')

m = {}
m_0_0 = solver.IntVar(0, 10, "0_0")
m_0_1 = solver.IntVar(0, 10, "0_1")
m_1_0 = solver.IntVar(0, 1, "1_0")
m_1_1 = solver.IntVar(0, 1, "1_1")

s = {}
s['row1'] = solver.IntVar(0, 1, 's_row1')
s['row2'] = solver.IntVar(0, 1, 's_row2')
s['col1'] = solver.IntVar(0, 1, 's_col1')
s['col2'] = solver.IntVar(0, 1, 's_col2')

#solver.Add(solver.IsGreaterCstCt(m[(0,0)] + m[(0,1)], 2, s['row1']))
#solver.Add(solver.IsLessCstCt(m[(0,0)] + m[(0,1)], 3, s['row1']))
solver.Add(solver.SumGreaterOrEqual([m_0_0, m_0_1], 5))
solver.Add(solver.SumLessOrEqual([m_0_0, m_0_1], 10))
solver.Add(m_0_0 + m_1_1 >= m_1_0)
objective = solver.Minimize(m_0_0 + m_0_1, 1)

solution = solver.Assignment()
solution.Add(m_0_0)
solution.Add(m_0_1)
#solution.Add(s['row1'])
#solution.AddObjective(s['row1'])

db = solver.Phase(
    [m_0_0, m_0_1], 
    solver.CHOOSE_MIN_SIZE_LOWEST_MAX,
    solver.ASSIGN_MIN_VALUE)
solver.NewSearch(db, [objective])

print "starting..."
while solver.NextSolution():
  print "%s %s" % (m_0_0, m_0_1)
