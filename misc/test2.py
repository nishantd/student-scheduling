#!/usr/bin/python

import os, sys
from ortools.constraint_solver import pywrapcp

solver = pywrapcp.Solver('schedule')

NUM_FLEXBLOCKS = 5
MAX_STUDENTS_PER_FLEXBLOCK = 1

# List to enable counting number of students in flexblock.
flexblocks_counts = []
for i in range(NUM_FLEXBLOCKS):
  _tmp = [0]*NUM_FLEXBLOCKS
  _tmp[i] = 1
  flexblocks_counts.append(_tmp)
print "flexblocks_counts: %s" % (flexblocks_counts, )

sc = {}
sc[(0,0)] = solver.IntVar(0, NUM_FLEXBLOCKS-1, 'sc_0_0')
sc[(1,0)] = solver.IntVar(0, NUM_FLEXBLOCKS-1, 'sc_1_0')
sc[(2,0)] = solver.IntVar(0, NUM_FLEXBLOCKS-1, 'sc_2_0')
sc[(3,0)] = solver.IntVar(0, NUM_FLEXBLOCKS-1, 'sc_3_0')
sc[(4,0)] = solver.IntVar(0, NUM_FLEXBLOCKS-1, 'sc_4_0')

all_vars = [sc[student_id, 0] for student_id in range(5)]

for i in range(NUM_FLEXBLOCKS):
  solver.Add(MAX_STUDENTS_PER_FLEXBLOCK == solver.Sum(solver.Element(flexblocks_counts[i], sc[(student_id, 0)]) for student_id in range(5)))

solution = solver.Assignment()
solution.Add(all_vars)

db = solver.Phase(all_vars,     
                  solver.CHOOSE_MIN_SIZE_LOWEST_MAX,
                  solver.ASSIGN_MIN_VALUE)

solver.NewSearch(db)

print all_vars
print "starting..."

solution_num = 0
while solver.NextSolution():
  print "solution %i: %s" % (solution_num, all_vars,)
  solution_num += 1



