import os, sys
from ortools.constraint_solver import pywrapcp

num_students=20
#num_classes=1
num_flexblocks=4

solver = pywrapcp.Solver('schedule')

# A matrix with row_number = student_id and column_number = flexblocks.
# I.e. student_class_math[(3,5)] = 1 => student_id = 3 is taking the math
# class in slot 5. Domain of each element in the matrix is (0,1).
student_class_math = {}

# Create the matrix.
# Constraints: Each student takes the math class exactly once.
for student_id in range(num_students):
  for flexblock_id in range(num_flexblocks):
    student_class_math[(student_id, flexblock_id)] \
        = solver.IntVar(0, 1, 'sc(%i,%i)' % (student_id, flexblock_id))
  solver.Add(solver.Sum(student_class_math[(student_id, j)] for j in range(num_flexblocks)) == 1) 

# Variables for each flexblock representing the number of students per flexblock.
# Also a slack variable to allow optimization on the number of students per flexblock.
# Hard-code the ideal number of students per flexblock.
IDEAL_STUDENTS_PER_FLEXBLOCK = 5;
flexblocks = []
flexblocks_slack = []
for flexblock_id in range(num_flexblocks):
  flexblocks.append(solver.IntVar(0, num_students, 'fb(%i)' % (flexblock_id,)))
  solver.Add(solver.Sum(student_class_math[(i, flexblock_id)] for i in range(num_students)) == flexblocks[flexblock_id])
  flexblocks_slack.append(solver.IntVar(0, num_students, 'fbs(%i)' % (flexblock_id,)))
  solver.Add(abs(flexblocks[flexblock_id] - IDEAL_STUDENTS_PER_FLEXBLOCK) - flexblocks_slack[flexblock_id] == 0)
  #solver.Add(solver.Sum(student_class_math[(i, flexblock_id)] for i in range(num_students)) <= 7)
  #solver.Add(solver.Sum(student_class_math[(i, flexblock_id)] for i in range(num_students)) >= 3)
total_slack = solver.IntVar(0, num_students, 'total_slack')
solver.Add(total_slack == solver.Sum(flexblocks_slack[flexblock_id] for flexblock_id in range(num_flexblocks)))

objective = solver.Minimize(total_slack, 1)

c_flat = [student_class_math[(student_id, flexblock_id)] \
    for student_id in range(num_students) \
        for flexblock_id in range(num_flexblocks)]

solution = solver.Assignment()
solution.Add(c_flat)
solution.Add(flexblocks)
solution.Add(flexblocks_slack)
solution.Add(total_slack)
solution.AddObjective(total_slack)

db = solver.Phase(c_flat, solver.CHOOSE_FIRST_UNBOUND, solver.ASSIGN_CENTER_VALUE)
solver.NewSearch(db, [objective])

# Seems like a good idea to try a very constrainted solution first. If infeasible,
# then loosen the slack. Otherwise the search will take a long time to get to the
# optimal solution without the propagation failures.
#total_slack.SetValue(0)
total_slack.SetRange(0,2)

num_solutions = 0
while solver.NextSolution():
  num_solutions += 1
  print 'SOLUTION: %i, total_slack: %i' % (num_solutions, total_slack.Value())
  for student_id in range(num_students):
    print [student_class_math[(student_id, flexblock_id)].Value() for flexblock_id in range(num_flexblocks)]
    print ""
  print "\n\n"
  #break

solver.EndSearch()

print
print 'num_solutions:', num_solutions
print 'failures:', solver.Failures()
print 'branches:', solver.Branches()
print 'WallTime:', solver.WallTime(), 'ms'
