#!/usr/bin/python

import optparse
import os, sys
from ortools.constraint_solver import pywrapcp

solver = pywrapcp.Solver('schedule')

num_students = None  # Total number of students.
num_flexblocks = None  # Total number of flexblocks in the week.
num_classes = None  # Total number of classes (i.e. math1, history2, english1, math2,..)
IDEAL_STUDENTS_PER_FLEXBLOCK = None
student_groups_size = None

flexblocks_element_mask = []

def _create_flexblocks_element_mask():
  global flexblocks_element_mask
  for i in range(num_flexblocks):
    _tmp = [0]*num_flexblocks
    _tmp[i] = 1
    flexblocks_element_mask.append(_tmp)

def _flatten_cube(cube):
  return [cube[(i, j)] for i in range(num_students) for j in range(num_classes)]

def create_class_student_flexblocks():
  # Create the matrices for each class.
  students_classes = {}
  for class_id in range(num_classes):
    for student_id in range(num_students):
        students_classes[(student_id, class_id)] \
            = solver.IntVar(0, num_flexblocks-1, 'sc(%i,%i)' % (student_id, class_id))
  return students_classes

def make_same(student_id_1, student_id_2, students_classes):
  for class_id in range(num_classes):
      solver.Add(students_classes[(student_id_1, class_id)] == students_classes[(student_id_2, class_id)])

def create_constraints(students_classes):
  # Constraints:
  # 1. Sum of students in a class_flexblock - class_flexblock_slack <= ideal number of students in a class
  # 2. Student should not be double-booked (with a slack)

  # Number of students per flexblock (per class)
  all_flexblocks_slacks = []
  for class_id in range(num_classes):
    flexblocks_slack = []
    for flexblock_id in range(num_flexblocks):
      _slack = solver.IntVar(0, num_students, 'class_%i_flexblock_%i_slack' % (class_id, flexblock_id))
      flexblocks_slack.append(_slack)
      solver.Add(solver.Sum(solver.Element(flexblocks_element_mask[flexblock_id], students_classes[(student_id, class_id)]) for student_id in range(num_students)) - _slack <= IDEAL_STUDENTS_PER_FLEXBLOCK)
    all_flexblocks_slacks.extend(flexblocks_slack)

    all_mask = [1] * num_flexblocks
    solver.Add(solver.Sum(solver.Element(all_mask, students_classes[(student_id, class_id)]) for student_id in range(num_students)) == num_students)

  # Students should not be double-booked
  for student_id in range(num_students):
    student_schedule = [students_classes[(student_id, class_id)] for class_id in range(num_classes)]
    solver.Add(solver.AllDifferent(student_schedule))

  return {'all_flexblocks_slacks':all_flexblocks_slacks}

def do_search():
  students_classes = create_class_student_flexblocks()
  new_vars_dict = create_constraints(students_classes)

  all_slacks = []
  all_slacks.extend(new_vars_dict['all_flexblocks_slacks'])
  #print >> sys.stderr, "all_slacks: %s" % (all_slacks,)
  z = solver.Sum(all_slacks)

  #for sv in slack_vars:
  #  objective = solver.Minimize(sv, 1)
  objective = solver.Minimize(z, 1)

  solution = solver.Assignment()
  students_classes_array = _flatten_cube(students_classes)
  solution.Add(students_classes_array)
  solution.Add(all_slacks)
  solution.Add(z)
  solution.AddObjective(z)

  db = solver.Phase(
      students_classes_array + all_slacks, 
      #solver.CHOOSE_MIN_SIZE_LOWEST_MAX, 
      solver.CHOOSE_DYNAMIC_GLOBAL_BEST,
      solver.ASSIGN_MIN_VALUE)
  solver.NewSearch(db, [objective])

  for flexblock_slacks in new_vars_dict['all_flexblocks_slacks']:
    flexblock_slacks.SetValue(0)
    #flexblock_slacks.SetRange(0,4)

  # Create student groups. This *really* is helpful for propagation.
  if student_groups_size and student_groups_size > 1:
    print >> sys.stderr, "making student groups of size %s" % (student_groups_size,)
    for student_id in range(num_students):
      if student_id % student_groups_size != 0:
        make_same((student_id / student_groups_size) * student_groups_size,
                  student_id,
                  students_classes)

  num_solutions = 0
  #print >> sys.stderr, "all_slacks 2: %s" % (all_slacks,)
  while solver.NextSolution():
    assert z.Bound(), "z is not bound?!?!"
    num_solutions += 1
    print "SOLUTION: %i, z: %s" % (num_solutions, z.Max())
    print "Flexblocks slacks:"
    for fs in new_vars_dict['all_flexblocks_slacks']:
      if fs.Value() != 0: print fs
    print "Done Flexblocks slacks"

    for class_id in range(num_classes):
      print 'CLASS %i:' % (class_id,)
      print [students_classes[(student_id, class_id)].Value() for student_id in range(num_students)]
      print "\n"
    sys.stdout.flush()

    # Check if z is minimized, if it is then break
    if z.Max() == 0:
      print "Minimized z: %s" % (z.Max(),)
      break

  print
  print 'num_solutions:', num_solutions
  print 'failures:', solver.Failures()
  print 'branches:', solver.Branches()
  print 'WallTime:', solver.WallTime(), 'ms'
  return num_solutions


def main():
  global num_students
  global num_flexblocks
  global num_classes
  global IDEAL_STUDENTS_PER_FLEXBLOCK
  global student_groups_size

  parser = optparse.OptionParser()
  parser.add_option('--num-students', type="int", dest="num_students",
                    action="store")
  parser.add_option('--num-flexblocks', type='int', dest='num_flexblocks', action='store')
  parser.add_option('--num-classes', type='int', dest='num_classes', action='store')
  parser.add_option('--students-per-flexblock', type='int', dest='students_per_flexblock',
                    action='store')
  parser.add_option('--student-groups-size', dest='student_groups_size', type='int',
                    action='store', default=None)

  (options, args) = parser.parse_args()

  num_students = options.num_students
  num_flexblocks = options.num_flexblocks
  num_classes = options.num_classes
  IDEAL_STUDENTS_PER_FLEXBLOCK = options.students_per_flexblock
  student_groups_size = options.student_groups_size
  _create_flexblocks_element_mask()

  print >> sys.stderr, "num_students=%s num_flexblocks=%s num_classes=%s, IDEAL_STUDENTS_PER_FLEXBLOCK=%s" \
      % (num_students, num_flexblocks, num_classes, IDEAL_STUDENTS_PER_FLEXBLOCK)

  assert num_classes <= num_flexblocks,  \
      "num_classes (%i) cannot be larger than num_flexblocks (%i)" % (num_classes, num_flexblocks)
  do_search()


if __name__ == "__main__":
  main()

# ./cp2.py --num-students=40 --num-flexblocks=10 --num-classes=7 --students-per-flexblock=4
# num_solutions: 1, failures: 9396390, branches: 18792012, WallTime: 272389 ms

# ./cp2.py --num-students=40 --num-flexblocks=10 --num-classes=10 --students-per-flexblock=4
# num_solutions: 1, failures: 9396426, branches: 18792048, WallTime: 450251 ms
# num_solutions: 1, failures: 9395622, branches: 18792048, WallTime: 486347 ms

