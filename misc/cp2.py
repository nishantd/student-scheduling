#!/usr/bin/python

import optparse
import os, sys
from ortools.constraint_solver import pywrapcp

solver = pywrapcp.Solver('schedule')

num_students = None  # Total number of students.
num_flexblocks = None  # Total number of flexblocks in the week.
num_classes = None  # Total number of classes (i.e. math1, history2, english1, math2,..)
IDEAL_STUDENTS_PER_FLEXBLOCK = None

def _flatten_cube(cube):
  return [cube[i][(j, k)] for i in range(num_classes) for j in range(num_students) for k in range(num_flexblocks)]

def create_class_student_flexblocks_cube():
  # Create the matrices for each class.
  student_classes = []
  for class_id in range(num_classes):
    student_class = {}
    student_classes.append(student_class)
    for student_id in range(num_students):
      for flexblock_id in range(num_flexblocks):
        student_class[(student_id, flexblock_id)] \
            = solver.IntVar(0, 1, 'sc(%i,%i)' % (student_id, flexblock_id))
  return student_classes

def create_constraints(student_classes):
  # Constraints:
  # 1. Sum of students in a class_flexblock - class_flexblock_slack <= ideal number of students in a class
  # 2. Student should not be double-booked (with a slack)
  # 3. Each student takes a class exactly once.

  # Constraint 3: Each student takes a class exactly once.
  for class_id in range(num_classes):
    for student_id in range(num_students):
      solver.Add(solver.Sum(student_classes[class_id][(student_id, j)] for j in range(num_flexblocks)) == 1)

  # Number of students per flexblock (per class)
  all_flexblocks = []
  all_flexblocks_slacks = []
  for class_id in range(num_classes):
    flexblocks = []
    flexblocks_slack = []
    for flexblock_id in range(num_flexblocks):
      flexblocks.append(solver.IntVar(0, num_students, 'class_%i_flexblock_%i' % (class_id, flexblock_id,)))
      solver.Add(solver.Sum(student_classes[class_id][(i, flexblock_id)] for i in range(num_students)) == flexblocks[flexblock_id])
      flexblocks_slack.append(solver.IntVar(0, max(IDEAL_STUDENTS_PER_FLEXBLOCK, num_students), 'class_%i_flexblock_%i_slack' % (class_id, flexblock_id)))
      #solver.Add(abs(flexblocks[flexblock_id] - IDEAL_STUDENTS_PER_FLEXBLOCK) - flexblocks_slack[flexblock_id] == 0)
      # Constraint 1.
      #solver.Add(flexblocks[flexblock_id] - flexblocks_slack[flexblock_id] <= IDEAL_STUDENTS_PER_FLEXBLOCK)
      solver.Add(flexblocks[flexblock_id] - flexblocks_slack[flexblock_id] == IDEAL_STUDENTS_PER_FLEXBLOCK)
      #solver.Add(solver.IsLessCt(flexblocks[flexblock_id] - flexblocks_slack[flexblock_id], IDEAL_STUDENTS_PER_FLEXBLOCK + 1, solver.IntVar(1, 1, "")))
    # This constraint is important for propagation to find there is no solution.
    solver.Add(solver.Sum(flexblocks) == num_students)
    all_flexblocks.extend(flexblocks)
    all_flexblocks_slacks.extend(flexblocks_slack)

  # Students should not be double-booked (with a slack variable)
  student_double_booked_slacks = []
  for student_id in range(num_students):
    for flexblock_id in range(num_flexblocks):
      sl = solver.IntVar(
          0, num_classes, 'student_%i_flexblock_%i_slack' % (student_id, flexblock_id))
      student_double_booked_slacks.append(sl)
      # This constraint does not allow the right solution to minimize sl (as sl can be 1).
      # It is important to allow the final solution to minimize the objective, otherwise
      # the solver will keep trying to minimize.
      #solver.Add(abs(solver.Sum(student_classes[class_id][(student_id, flexblock_id)] for class_id in range(num_classes)) - 1) == sl)
      # This one does the trick.
      #solver.Add(solver.IsGreaterCstCt(solver.Sum(student_classes[class_id][(student_id, flexblock_id)] for class_id in range(num_classes)), 1, sl))
      #solver.Add(solver.SumLessOrEqual(student_classes[class_id][(student_id, flexblock_id)] for class_id in range(num_classes), 1))
      # Constraint 2: Student should not be double booked.
      solver.Add(solver.Sum(student_classes[class_id][(student_id, flexblock_id)] for class_id in range(num_classes)) -1 <= sl)
      #solver.Add(solver.Sum(student_classes[class_id][(student_id, flexblock_id)] for class_id in range(num_classes)) <= 1)
  return {'student_double_booked_slacks':student_double_booked_slacks,
          'all_flexblocks_slacks':all_flexblocks_slacks,
          'all_flexblocks':all_flexblocks}

def main():
  global num_students
  global num_flexblocks
  global num_classes
  global IDEAL_STUDENTS_PER_FLEXBLOCK

  parser = optparse.OptionParser()
  parser.add_option('--num-students', type="int", dest="num_students",
                    action="store")
  parser.add_option('--num-flexblocks', type='int', dest='num_flexblocks', action='store')
  parser.add_option('--num-classes', type='int', dest='num_classes', action='store')
  parser.add_option('--students-per-flexblock', type='int', dest='students_per_flexblock',
                    action='store')

  (options, args) = parser.parse_args()

  num_students = options.num_students
  num_flexblocks = options.num_flexblocks
  num_classes = options.num_classes
  IDEAL_STUDENTS_PER_FLEXBLOCK = options.students_per_flexblock

  print >> sys.stderr, "num_students=%s num_flexblocks=%s num_classes=%s, IDEAL_STUDENTS_PER_FLEXBLOCK=%s" \
      % (num_students, num_flexblocks, num_classes, IDEAL_STUDENTS_PER_FLEXBLOCK)

  assert num_classes <= num_flexblocks,  \
      "num_classes (%i) cannot be larger than num_flexblocks (%i)" % (num_classes, num_flexblocks)
  student_classes = create_class_student_flexblocks_cube()
  #(new_vars, total_slack) = create_constraints(student_classes)
  new_vars_dict = create_constraints(student_classes)

  all_slacks = []
  all_slacks.extend(new_vars_dict['student_double_booked_slacks'])
  all_slacks.extend(new_vars_dict['all_flexblocks_slacks'])
  print >> sys.stderr, "all_slacks: %s" % (all_slacks,)
  z = solver.Sum(all_slacks)

  #for sv in slack_vars:
  #  objective = solver.Minimize(sv, 1)
  objective = solver.Minimize(z, 1)

  solution = solver.Assignment()
  student_classes_array = _flatten_cube(student_classes)
  solution.Add(student_classes_array)
  solution.Add(new_vars_dict['all_flexblocks'])
  solution.Add(all_slacks)
  solution.Add(z)
  solution.AddObjective(z)

  db = solver.Phase(
      student_classes_array + all_slacks, solver.CHOOSE_MIN_SIZE_LOWEST_MAX, solver.ASSIGN_MIN_VALUE)
  solver.NewSearch(db, [objective])

  #total_slack.SetValue(0)
  for sv in new_vars_dict['student_double_booked_slacks']:
    #sv.SetRange(0,1)
    sv.SetValue(0)
  for flexblock_slacks in new_vars_dict['all_flexblocks_slacks']:
    flexblock_slacks.SetValue(0)
    #flexblock_slacks.SetRange(0,4)
  for flexblock in new_vars_dict['all_flexblocks']:
    flexblock.SetRange(0, IDEAL_STUDENTS_PER_FLEXBLOCK)
  num_solutions = 0
  print >> sys.stderr, "all_slacks 2: %s" % (all_slacks,)
  while solver.NextSolution():
    assert z.Bound(), "z is not bound?!?!"
    num_solutions += 1
    #print 'SOLUTION: %i, %s' % (num_solutions, new_vars_dict)
    #print 'z: %s' % (z,)
    print "SOLUTION: %i, z: %s" % (num_solutions, z.Max())
    #print "Student double booked slacks:"
    #for sv in new_vars_dict['student_double_booked_slacks']:
    #  if sv.Value() != 0: print sv
    #print "Done Student double booked slacks"
    print "Flexblocks slacks:"
    for fs in new_vars_dict['all_flexblocks_slacks']:
      if fs.Value() != 0: print fs
    print "Done Flexblocks slacks"

    for class_id in range(num_classes):
      print 'CLASS %i:' % (class_id,)
      for student_id in range(num_students):
        print [student_classes[class_id][(student_id, flexblock_id)].Value() for flexblock_id in range(num_flexblocks)]
        print ""
      print "\n\n"
      sys.stdout.flush()
      #break
    
    # Check if z is minimized, if it is then break
    if z.Max() == 0:
      print "Minimized z: %s" % (z.Max(),)
      break

  print
  print 'num_solutions:', num_solutions
  print 'failures:', solver.Failures()
  print 'branches:', solver.Branches()
  print 'WallTime:', solver.WallTime(), 'ms'

if __name__ == "__main__":
  main()

# ./cp2.py --num-students=40 --num-flexblocks=10 --num-classes=7 --students-per-flexblock=4
# num_solutions: 1, failures: 9396390, branches: 18792012, WallTime: 272389 ms

# ./cp2.py --num-students=40 --num-flexblocks=10 --num-classes=10 --students-per-flexblock=4
# num_solutions: 1, failures: 9396426, branches: 18792048, WallTime: 450251 ms
# num_solutions: 1, failures: 9395622, branches: 18792048, WallTime: 486347 ms

