#!/usr/bin/python

import os, sys

import pickle

from ortools.constraint_solver import pywrapcp

import school
import create_school
import create_schedule
import stats

def _flatten_cube(cube):
  return [cube[(i, j)] for i in range(num_students) for j in range(num_classes)]

def _print_sorted(a):
  _tmp = ['%s'%(e,) for e in a]
  _tmp.sort()
  print '\n'.join(e for e in _tmp)

def _sort_intvar_array(a):
  _tmp = [(v.Value(), v) for v in a]
  return [v for (x,v) in _tmp]

def _print_flexblock_stats(students_classes_array, flexblocks_array):
  flexblock_students = {}
  for v in students_classes_array:
    x = flexblock_students.setdefault(v.Value(), [0])
    x[0] += 1
  flexblocks_stats = [(flexblock_index,v) for (flexblock_index,v) in flexblock_students.items()]
  flexblocks_stats.sort()
  for i in range(len(flexblocks_stats)):
    (flexblock_index, num_students) = flexblocks_stats[i]
    class_index = flexblocks_array[flexblock_index].Value()
    print 'flexblock_index=%s class_index=%s num_students=%s' \
        % (flexblock_index, class_index, num_students)
  #print '\n'.join('flexblock_index=%s num_students=%s'%(k,v) for (k,v) in flexblocks_stats)

def do_search():
  read_pk = os.getenv('READ_PK','no').lower()
  if read_pk != 'yes':
    school = create_school.create_school()
    f = open('/tmp/school.pk','wb')
    pickle.dump(school, f)
    f.close()
    s = school
  else:
    f = open('/tmp/school.pk', 'rb')
    s = pickle.load(f)

  assert s.classes_dict
  assert s.students_dict
  assert s.flexblocks_dict
  assert s.subjects_dict

  create_school.to_str(s, sys.stderr)
  class_stats = stats.classes_flexblocks(s.students_dict)
  _tmp = ["%s: num_students=%s num_flexblocks_required=%s" \
            % (k, v[0], v[1]) for (k,v) in class_stats.iteritems()]
  _tmp.sort()
  print >> sys.stderr, "class stats:"
  print >> sys.stderr, '\n'.join('%s'%(e,) for e in _tmp)

  solver = pywrapcp.Solver('schedule')
  (students_classes, flexblocks_array) = create_schedule.create_schedule(s, solver)
  #create_schedule.print_students_classes_by_class(
  #    students_classes, s.classes_dict, s.students_dict, sys.stderr)
  new_vars_dict = create_schedule.create_constraints(
      solver, students_classes, flexblocks_array, s)

  all_slacks = []
  all_slacks.extend(new_vars_dict['max_flexblocks_slacks'])
  all_slacks.extend(new_vars_dict['min_flexblocks_slacks'])
  print >> sys.stderr, "all_slacks: %s" % (all_slacks,)
  z = solver.Sum(all_slacks)

  objective = solver.Minimize(z, 1)

  solution = solver.Assignment()
  students_classes_array = []
  for v_list in students_classes.values():
    if not v_list: continue
    students_classes_array.extend(v_list)
  #students_classes_array = [v for v in students_classes.values() if v]
  solution.Add(students_classes_array)
  #solution.Add(flexblocks_array)
  #solution.Add(new_vars_dict['min_constraints_rhs'])
  solution.Add(all_slacks)
  solution.Add(z)
  solution.AddObjective(z)

  db = solver.Phase(
      #students_classes_array + flexblocks_array + all_slacks, 
      students_classes_array + all_slacks,
      #solver.CHOOSE_MIN_SIZE_LOWEST_MAX, 
      solver.CHOOSE_DYNAMIC_GLOBAL_BEST,
      #solver.CHOOSE_STATIC_GLOBAL_BEST,
      solver.ASSIGN_MIN_VALUE
      #solver.ASSIGN_MAX_VALUE
      #solver.ASSIGN_RANDOM_VALUE
      #solver.ASSIGN_CENTER_VALUE
      )
  solver.NewSearch(db, [objective])

  #for flexblock_slacks in new_vars_dict['max_flexblocks_slacks']:
  #  flexblock_slacks.SetValue(0)

  #for flexblock_slacks in new_vars_dict['min_flexblocks_slacks']:
  #  flexblock_slacks.SetValue(0)

  print "FLEXBLOCKS ARRAY:"
  _print_sorted(flexblocks_array)

  print "STUDENT CLASSES ARRAY:"
  _print_sorted(students_classes_array)

  num_solutions = 0
  #print >> sys.stderr, "all_slacks 2: %s" % (all_slacks,)
  sys.stdout.flush()
  sys.stderr.flush()
  while solver.NextSolution():
    assert z.Bound(), "z is not bound?!?!"
    num_solutions += 1
    print "SOLUTION: %i, z: %s" % (num_solutions, z.Max())
    print >> sys.stderr, "SOLUTION: %i, z: %s" % (num_solutions, z.Max())
    def _cmp(x, y): 
        return cmp(x.overall_class_index, y.overall_class_index)
    classes = s.classes_dict.values()
    classes.sort(cmp=_cmp)
    print "Classes:"
    print '\n'.join('%s'%(c,) for c in classes)
    print "Max Flexblocks slacks:"
    for fs in new_vars_dict['max_flexblocks_slacks']:
      if fs.Value() != 0: print fs
    print "Min Flexblocks slacks:"
    for fs in new_vars_dict['min_flexblocks_slacks']:
      if fs.Value() != 0: print fs
    print "Done Flexblocks slacks"
    print "scheduled flexblocks:"
    _print_sorted(flexblocks_array)
    print "students per flexblock:"
    _print_flexblock_stats(students_classes_array, flexblocks_array)
    print "scheduled students_classes:"
    _print_sorted(students_classes_array)
    print "Done scheduled flexblocks"
    sys.stdout.flush()

  print "NUM SOLUTIONS: %s" % (num_solutions, )


if __name__ == "__main__":
  #main()
  do_search()

