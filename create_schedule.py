#!/usr/bin/python

import optparse
import os, sys
from ortools.constraint_solver import pywrapcp

"""Given a school, create a schedule, and return the enhanced school objects.
"""

MAX_STUDENTS_PER_FLEXBLOCK=int(os.getenv('MAX_STUDENTS',20))
ENFORCE_CLASS_ORDER = int(os.getenv('ENFORCE_CLASS_ORDER', 1))
MIN_STUDENTS_PER_FLEXBLOCK=int(os.getenv('MIN_STUDENTS',0))
ENFORCE_SUBJECT_CLASS_ORDER = int(os.getenv('ENFORCE_SUBJECT_CLASS_ORDER', 1))

def _create_flexblocks_element_mask(num_flexblocks):
  print >> sys.stderr, "_create_flexblocks_element_mask(%s)" % (num_flexblocks,)
  flexblocks_element_mask = []
  for i in range(num_flexblocks):
    _tmp = [0]*num_flexblocks
    _tmp[i] = 1
    flexblocks_element_mask.append(_tmp)
  return flexblocks_element_mask

def _create_flexblocks_day_index_mask(flexblocks):
  flexblocks_day_index_mask = []
  sorted_flexblocks = flexblocks
  def _cmp(f1, f2): return cmp(f1.overall_flexblock_index, f2.overall_flexblock_index)
  sorted_flexblocks.sort(cmp=_cmp)
  for flexblock in sorted_flexblocks:
    flexblocks_day_index_mask.append(flexblock.day_index)
  return flexblocks_day_index_mask

def _create_flexblocks_timeslot_index_mask(flexblocks):
  flexblocks_timeslot_index_mask = []
  sorted_flexblocks = flexblocks
  def _cmp(f1, f2): return cmp(f1.overall_flexblock_index, f2.overall_flexblock_index)
  sorted_flexblocks.sort(cmp=_cmp)
  for flexblock in sorted_flexblocks:
    flexblocks_timeslot_index_mask.append(flexblock._index)
  return flexblocks_timeslot_index_mask

def _trim_class_student_flexblock_domain(flexblocks, valid_flexblock_names, v):
  """flexblocks: {flexblock_name:flexblock,...}
  valid_flexblock_names: valid flexblocks for this class
  v: the IntVar whose domain must be trimmed.
  """
  print >> sys.stderr, "_trim_class_student_flexblock_domain called."
  print >> sys.stderr, "flexblocks: %s" % (','.join(fb.name for fb in flexblocks.values()),)
  print >> sys.stderr, "valid_flexblock_names: %s" % (valid_flexblock_names,)
  print >> sys.stderr, "v: %s" % (v,)
  sorted_flexblocks = flexblocks.values()
  def _cmp(f1, f2): return cmp(f1.overall_flexblock_index, f2.overall_flexblock_index)
  sorted_flexblocks.sort(cmp=_cmp)
  for flexblock in sorted_flexblocks:
    if flexblock.name not in valid_flexblock_names:
      print >> sys.stderr, "removing value %s" % (flexblock._index,)
      v.RemoveValue(flexblock.overall_flexblock_index)
  print >> sys.stderr, "returning with v: %s" % (v,)

def create_class_student_flexblocks(solver, classes, students, flexblocks, subjects):
  """classes: {class_name:class,...}
  students: {student_name:student}
  flexblocks: {flexblock_name:flexblock}
  subjects: {subject_name:subject}
  """
  print >> sys.stderr, "create_class_student_flexblocks called"
  # Create the matrices for each class.
  def _cmp(x, y): return cmp(x._index, y._index)
  students_ordered_list = students.values()
  students_ordered_list.sort(cmp=_cmp)
  def _cmp2(x, y): return cmp(x.overall_class_index, y.overall_class_index)
  classes_ordered_list = classes.values()
  classes_ordered_list.sort(cmp=_cmp2)
  students_classes = {}
  for _class in classes_ordered_list:
    print >> sys.stderr, "for class: %s" % (_class,)
    for student in students_ordered_list:
      print >> sys.stderr, "for student: %s" % (student.name,)
      required_classes = {}
      for (required_class_name, num_flexblocks, consecutive) in student.required_classes:
        print >> sys.stderr, "required_class_name=%s, num_flexblocks=%s, consecutive=%s" \
            % (required_class_name, num_flexblocks, consecutive)
        # classes must have required_class_name - otherwise fail.
        required_classes[required_class_name] = num_flexblocks
      num_flexblocks = required_classes.get(_class.name, 0)
      if num_flexblocks > 0:
        v_list = []
        for _f in range(num_flexblocks):
          v = solver.IntVar(
              0, len(flexblocks)-1, 
              'sc(%i,%i,%i)' % (student._index, _class.overall_class_index, _f))
          subject = subjects[_class.subject_name]
          _trim_class_student_flexblock_domain(flexblocks, subject.flexblock_names, v)
          v_list.append(v)
      else:
        v_list = None
      students_classes[(student._index, _class.overall_class_index)] = v_list
  print >> sys.stderr, "returning students_classes"
  return students_classes

def print_students_classes_by_class(students_classes, classes, students, stream):
  def _cmp(x, y): return cmp(x.overall_class_index, y.overall_class_index)
  sorted_classes = classes.values()
  sorted_classes.sort(cmp=_cmp)
  def _cmp2(x, y): return cmp(x._index, y._index)
  sorted_students = students.values()
  sorted_students.sort(cmp=_cmp2)
  for _class in sorted_classes:
    print >> stream, "\nSCHEDULE FOR CLASS: %s" % (_class,)
    s = ''
    for student in sorted_students:
      v_list = students_classes[(student._index, _class.overall_class_index)]
      if v_list:
        print >> stream, '\tstudent %s: %s'%(student.name, '|'.join(v_list))
      

def create_schedule(school, solver):
  students_classes = create_class_student_flexblocks(
      solver,
      school.classes_dict, school.students_dict, school.flexblocks_dict, school.subjects_dict)
  flexblocks_array = create_flexblocks_array(
      solver, school.classes_dict, school.flexblocks_dict)
  return (students_classes, flexblocks_array)
  #print >> sys.stderr, "%s" % (students_classes.values(),)

def create_flexblocks_array(solver, classes, flexblocks):
  """Returns an array of IntVars, one for each flxblock, whose value
  should be the class scheduled for that flexblock.
  """
  print >> sys.stderr, "create_flexblocks_array called"
  def _cmp(x, y): return cmp(x.overall_flexblock_index, y.overall_flexblock_index)
  flexblocks_ordered_list = flexblocks.values()
  flexblocks_ordered_list.sort(cmp=_cmp)
  def _cmp2(x, y): return cmp(x.overall_class_index, y.overall_class_index)
  classes_ordered_list = classes.values()
  classes_ordered_list.sort(cmp=_cmp2)
  
  flexblocks_array = []
  for fb in flexblocks_ordered_list:
    if fb and fb.is_valid:
      fb_i = solver.IntVar(-1, classes_ordered_list[-1].overall_class_index, 
                            'fb_%i' % (fb.overall_flexblock_index,))
    else:
      fb_i = solver.IntVar(-1, -1, 'fb_%i' % (fb.overall_flexblock_index,))
    flexblocks_array.append(fb_i)
  print >> sys.stderr, "returning flexblocks_array"
  return flexblocks_array

def create_constraints(
  solver,
  students_classes, flexblocks_array, school,
  ideal_students_per_flexblock=MAX_STUDENTS_PER_FLEXBLOCK):
  """Returns all new variables created as part of the constraints.
  """
  print >> sys.stderr, "create_constraints called"
  classes = school.classes_dict.values()
  students = school.students_dict.values()
  flexblocks = school.flexblocks_dict.values()
  max_overall_index = max(_f.overall_flexblock_index for _f in flexblocks)
  flexblocks_element_mask = _create_flexblocks_element_mask(max_overall_index+1)

  # Constraints:
  # 1. Sum of students in a class_flexblock - class_flexblock_slack <= ideal number of students in a class
  # 2. Student should not be double-booked (with a slack)

  # Number of students per flexblock (per class)
  max_flexblocks_slacks = []
  min_flexblocks_slacks = []
  min_constraints_rhs = []
  for flexblock in flexblocks:
    if not flexblock.is_valid: continue
    flexblock_element_mask = flexblocks_element_mask[flexblock.overall_flexblock_index]
    students_in_flexblock = []
    for _class in classes:
      # TODO: Optimize here by only iterating on classes valid for this flexblock.
      for student in students:
        v_list = students_classes[(student._index, _class.overall_class_index)]
        if not v_list: continue
        for v in v_list:
          students_in_flexblock.append(solver.Element(flexblock_element_mask, v))
    if not students_in_flexblock: continue
    _slack = solver.IntVar(
      0, max(len(students) - ideal_students_per_flexblock, 0), 'flexblock_%i_slack' % \
        (flexblock.overall_flexblock_index))
    #_slack = solver.IntVar(
    #  0, 5, 'flexblock_%i_slack' % \
    #    (flexblock.overall_flexblock_index))
    max_flexblocks_slacks.append(_slack)
    solver.Add(solver.Sum(students_in_flexblock) - _slack <= ideal_students_per_flexblock)
    if MIN_STUDENTS_PER_FLEXBLOCK:
      print >> sys.stderr, "creating min_slack for flexblock: %s" % (flexblock,)
      _min_slack = solver.IntVar(
        0, MIN_STUDENTS_PER_FLEXBLOCK, 'flexblock_%i_min_slack' % \
          (flexblock.overall_flexblock_index))
      min_flexblocks_slacks.append(_min_slack)
      min_constraint_rhs = solver.IntVar(
        0, len(students), 'min_constraint_fb_%s' % (flexblock.overall_flexblock_index,))
      min_constraints_rhs.append(min_constraint_rhs)
      for i in range(1,MIN_STUDENTS_PER_FLEXBLOCK):
        min_constraint_rhs.RemoveValue(i)
      print >> sys.stderr, "created min_constraint_rhs: %s" % (min_constraint_rhs,)
      solver.Add(solver.Sum(students_in_flexblock) + _min_slack >= min_constraint_rhs)
      solver.Add(min_constraint_rhs >= solver.Sum(students_in_flexblock))
  print >> sys.stderr, "done looping for slack constraints"

  # Students should not be double-booked. I.e. have 2 flexblocks that are at the same time.
  print >> sys.stderr, "students no double booking"
  flexblocks_timeslot_index_mask = _create_flexblocks_timeslot_index_mask(flexblocks)
  for student in students:
    all_timeslots_for_student = []
    for _class in classes:
      v_list = students_classes[(student._index, _class.overall_class_index)]
      if not v_list: continue
      for i in range(1, len(v_list)):
        all_timeslots_for_student.append(
          solver.Element(flexblocks_timeslot_index_mask, v_list[i-1]))
    if all_timeslots_for_student:
      solver.Add(solver.AllDifferent(all_timeslots_for_student))
  print >> sys.stderr, "done students no double booking"

  # Students needing multiple flexblocks for classes should 
  # 1. Either have flexblocks on different days.
  # 2. Or they should be scheduled in consecutive flexblocks.
  flexblocks_day_index_mask = _create_flexblocks_day_index_mask(flexblocks)
  for student in students:
    # rc = {class_name:(num_flexblocks_required, consecutive),..}
    rc = dict((e[0], (e[1],e[2])) for e in student.required_classes)
    for _class in classes:
      v_list = students_classes[(student._index, _class.overall_class_index)]
      if not v_list or len(v_list) < 2: continue
      consecutive_required = rc[_class.name][1]
      if not consecutive_required:
        # Rather than using AllDifferent, use a strict '<' to narrow the search space.
        for i in range(1,len(v_list)):
          solver.Add(
            solver.Element(flexblocks_day_index_mask, v_list[i-1]) < solver.Element(flexblocks_day_index_mask, v_list[i]))
      else:
        for i in range(1,len(v_list)):
          solver.Add(v_list[i-1] + 1 == v_list[i])
          solver.Add(
            solver.Element(flexblocks_day_index_mask, v_list[i-1]) == solver.Element(flexblocks_day_index_mask, v_list[i]))

  # All classes must have different flexblocks and all students  
  print >> sys.stderr, "all classes must be in different flexblocks"
  for _class in classes:
    print >> sys.stderr, "class: %s" % (_class,)
    for student in students:
      print >> sys.stderr, "student: %s" % (student,)
      v_list = students_classes[(student._index, _class.overall_class_index)]
      if v_list:
        print >> sys.stderr, "adding all classes different flexblocks constraint"
        for v in v_list:
          solver.Add(solver.Element(flexblocks_array, v) == _class.overall_class_index)

  # For each student, schedule each subjects classes in increasing degree of difficulty.
  # This is optional.
  if ENFORCE_CLASS_ORDER:
    print >> sys.stderr, "enforcing class order for each subject"
    for student in students:
      print >> sys.stderr, "student: %s" % (student,)
      for (subject_name, subject) in school.subjects_dict.iteritems():
        print >> sys.stderr, "subject_name: %s" % (subject_name,)
        class_flexblocks = []
        for (class_name, num_flexblocks, consecutive) in student.required_classes:
          if class_name in subject.class_names:
            print >> sys.stderr, "student required class: %s" % (class_name,)
            _class = school.classes_dict[class_name]
            v_list  = students_classes[(student._index, _class.overall_class_index)]
            if not v_list: continue
            for v in v_list:
              class_flexblocks.append((_class._index, v))
        class_flexblocks.sort()
        print >> sys.stderr, ','.join('%s,%s' % (x,y) for (x,y) in class_flexblocks)
        for i in range(1,len(class_flexblocks)):
          #if class_flexblocks[i-1][0] < class_flexblocks[i][0]:
          #  print >> sys.stderr, "adding constraint between indexes: %s < %s" % \
          #      (class_flexblocks[i-1][0], class_flexblocks[i][0])
          solver.Add(class_flexblocks[i-1][1] < class_flexblocks[i][1])
      
  if ENFORCE_SUBJECT_CLASS_ORDER:
    print >> sys.stderr, "enforcing subject class order for each subject"
    for subject in school.subjects_dict.values():
      subject_flexblocks = []
      for flexblock_name in subject.flexblock_names:
        subject_flexblocks.append(school.flexblocks_dict[flexblock_name])
      def _cmp(x, y): return cmp(x._index, y._index)
      subject_flexblocks.sort(cmp=_cmp)
      # Sorting by flexblock._index is sorting by time
      # Now get the actual flexblock intvars from the flexblock array - also in time order
      flexblocks_var_array = []
      for fb in subject_flexblocks:
        flexblocks_var_array.append(flexblocks_array[fb.overall_flexblock_index])
      # Now add constraints
      for i in range(1,len(flexblocks_var_array)):
        solver.Add(flexblocks_var_array[i-1] <= flexblocks_var_array[i])

  print >> sys.stderr, "done creating constraints - returning slacks"
  return {'max_flexblocks_slacks':max_flexblocks_slacks,
          'min_flexblocks_slacks':min_flexblocks_slacks,
          'min_constraints_rhs':min_constraints_rhs}

