#!/usr/bin/python

import optparse
import os, sys

#from . import school
import school
import gdoc_util

STUDENT_PROGRESS_KEY = '0AtmVv0ZLTT8zdGx5YmptRElPVlJjenEwLTNERTRlM1E'
FLEXBLOCKS_KEY = '0AtmVv0ZLTT8zdGx5YmptRElPVlJjenEwLTNERTRlM1E'
WEEKLY_AVAILABILITY_SCHEDULE_SHEET = os.getenv(
    'WEEKLY_AVAILABILITY_SCHEDULE_SHEET',
    'weekly availability schedule')
WEEKLY_STUDENT_NEEDS_SHEET = os.getenv(
    'WEEKLY_STUDENT_NEEDS_SHEET',
    'weekly flexblock needs')

#SUBJECTS = ['English', 'Math', 'Physics', 'Econ']
#SUBJECTS = ['English', 'Math']
SUBJECTS = []

SUBJECTS_STR = os.getenv('SUBJECTS_STR', None)
if SUBJECTS_STR:
  SUBJECTS = [s.strip().lower() for s in SUBJECTS_STR.strip().split(',')]
print "SUBJECTS: %s" % (SUBJECTS,)

def _canonicalize(x):
  return x.lower()

def get_subjects_flexblocks(gdc, key, flexblock_sheet):
  """Knows how to read the teacher flexblock schedule.
  Returns ({subject_name:Subject,}, {flexblock_name:Flexblock,})
  All names are canonicalized.
  """
  flexblocks_per_day = int(os.getenv('FLEXBLOCKS_PER_DAY', 8))
  c = gdoc_util.cells(gdc, key, flexblock_sheet)
  print >> sys.stderr, "%s" % (c,)
  # Row 1 sets the number of columns to look at.
  num_columns = 0
  for cell in c[0][1:]:
    if cell is None: break
    num_columns += 1
    
  # Now the actual flexblocks and subjects
  subjects = {}
  flexblocks = {}
  overall_flexblock_index = 0
  subject_rows = {}
  for subject_row in c[1:]:
    if subject_row[0] is None: break
    print >> sys.stderr, "subject_row: %s" % (subject_row[0],)
    subject_name = _canonicalize(subject_row[0]).split('_')[0]
    if SUBJECTS and subject_name not in SUBJECTS: 
      print >> sys.stderr, "%s not in %s" % (subject_name, SUBJECTS)
      continue
    assert not subject_rows.has_key(subject_row[0])
    subject_rows[subject_row[0]] = None
    subject = subjects.get(subject_name)
    if subject is None:
      subject = school.Subject(
        name=subject_name, flexblock_names=[])
      subjects[subject_name] = subject
    index = 0
    for cell in subject_row[1:num_columns+1]:
      print >> sys.stderr, "overall_flexblock_index=%s, index=%s, cell=%s" % \
          (overall_flexblock_index, index, cell)
      flexblock_name = "%s_%s" % (_canonicalize(subject_row[0]), c[0][index+1])
      f = school.FlexBlock(name=flexblock_name, index=index, 
                           overall_flexblock_index=overall_flexblock_index)
      flexblocks[f.name] = f
      f.day_index = index / flexblocks_per_day
      if cell and cell.strip().lower() == 't':
        f.is_valid = True
        subject.flexblock_names.append(f.name)
      index += 1
      overall_flexblock_index += 1

  return (subjects, flexblocks)

def get_classes_students_info(gdc, key, sheet_name, subjects):
  """Knows how to read the student progress tracker.
  Input:
  subjects: dictionary of subjects with subject_name as key
  Returns (taken_classes={class_name:Class,}, students={student_name:Student,})
  """
  override_num_blocks = int(os.getenv('OVERRIDE_NUM_BLOCKS',0))
  # bool('0') = True, bool(int('0')) = False
  always_consecutive = bool(int(os.getenv('ALWAYS_CONSECUTIVE','0')))
  student_classes = set()
  students = {}
  subjects_classes = {}
  cells = gdoc_util.cells(gdc, key, sheet_name)
  # First row defines the number of columns to read.
  header_row = cells[0]
  assert header_row[0].lower().startswith('student')
  num_columns = 0
  for col in header_row[1:]:
    if not col: break
    num_columns += 1
  print >> sys.stderr, "num_columns = %i" % (num_columns,)
  student_index = 0
  for row in cells[1:]:
    if not row[0]: continue
    student_name = _canonicalize(row[0])
    student = school.Student(name=student_name, index=student_index, required_classes=[])
    students[student_name] = student
    for cell in row[1:num_columns+1]:
      if not cell: continue
      # format: subject/class/num_flexblocks[c=consecutive]
      _parts = cell.strip().split('/')
      assert len(_parts) >= 2 and len(_parts) <= 3, "bad cell format: %s" % (cell,)
      subject_name = _parts[0]
      assert subjects.has_key(subject_name)
      class_name = '%s-%s' % (subject_name, _parts[1])
      subject_classes = subjects_classes.setdefault(subject_name, set())
      num_flexblocks = 1
      consecutive = False
      if len(_parts) == 3:
        num_flexblocks = int(_parts[2][0])
        if len(_parts[2]) > 1:
          assert len(_parts[2]) == 2 and _parts[2][1].lower() == 'c', "bad cell parts format: %s" % (cell,)
          consecutive = True
      student.required_classes.append((class_name, num_flexblocks, consecutive))
      subject_classes.add(class_name)
  # Now build the classes dict
  taken_classes = {}
  overall_class_index = 0
  subjects_list = subjects.keys()
  subjects_list.sort()
  for subject in subjects_list:
    subject_classes = subjects_classes.get(subject)
    if not subject_classes:
      print >> sys.stderr, "no taken classes for subject %s" % (subject,)
      continue
    subject_classes_list = list(subject_classes)
    subject_classes_list.sort()
    subject_class_index = 0
    for subject_class in subject_classes_list:
      _class = school.Class(
          name=subject_class, index=subject_class_index,
          overall_class_index=overall_class_index,
          subject_name=subject)
      taken_classes[subject_class] = _class
      subject_class_index += 1
    overall_class_index += 1
  return (taken_classes, students)


def to_str(school_obj, stream):
  print >> stream, "-------------------------"
  print >> stream, "SCHOOL:"
  subjects = school_obj.subjects_dict
  print >> stream,  "subjects: %s\n%s" \
      % (len(subjects), '\n'.join(["%s:%s" % (k,v) for (k,v) in subjects.iteritems()]))
  flexblocks = school_obj.flexblocks_dict
  print >> stream, "flexblocks: %s\n%s" \
      % (len(flexblocks), '\n'.join(["%s:%s" % (k,v) for (k,v) in flexblocks.iteritems()]))
  classes = school_obj.classes_dict
  print >> stream, "classes: %s\n%s" \
      % (len(classes), '\n'.join(["%s:%s" % (k,v) for (k,v) in classes.iteritems()]))
  students = school_obj.students_dict
  print >> stream, "students: %s\n%s" \
      % (len(students), '\n'.join(["%s:%s" % (k,v) for (k,v) in students.iteritems()]))
  print >> stream, "-----------------------------\n"


def create_school():
  gdc = gdoc_util.gdClient()
  sheets = gdoc_util.sheets(gdc, FLEXBLOCKS_KEY) 
  flexblock_sheet = None
  student_needs_sheet = None
  for sheet in sheets:
    if sheet.title.text.lower() == WEEKLY_AVAILABILITY_SCHEDULE_SHEET.lower():
      flexblock_sheet = sheet
    if sheet.title.text.lower() == WEEKLY_STUDENT_NEEDS_SHEET.lower():
      student_needs_sheet = sheet
  assert flexblock_sheet, "must have flexblock_sheet (%s)" % (WEEKLY_AVAILABILITY_SCHEDULE_SHEET,)
  assert student_needs_sheet, "no student sheet found (%s)" % (WEEKLY_STUDENT_NEEDS_SHEET,)
  (subjects, flexblocks) = get_subjects_flexblocks(
      gdc, FLEXBLOCKS_KEY, flexblock_sheet)
  (classes, students) = get_classes_students_info(
      gdc, STUDENT_PROGRESS_KEY, student_needs_sheet, subjects)
  s = school.School(students, classes, subjects, flexblocks)
  return s

if __name__ == "__main__":
  s = create_school()
  to_str(s, sys.stdout)


