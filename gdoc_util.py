#!/usr/bin/python

import optparse
import os, sys

import gdata.service
import gdata.spreadsheet
import gdata.spreadsheet.service

GDATA_USERNAME="programmatic.api@gmail.com"
GDATA_PASSWORD="APIsarecool"

def gdClient():
  "init gdata client"
  gdc = gdata.spreadsheet.service.SpreadsheetsService()
  gdc.email = GDATA_USERNAME
  gdc.password = GDATA_PASSWORD
  #gdc.source = 'API User Task'
  gdc.ProgrammaticLogin()
  #gdc.debug = True
  return gdc

def sheets(gdc, key):
  doc = gdc.GetWorksheetsFeed( key )
  if doc is None:
    raise Exception( "no document found for key", key )
  return [e for e in doc.entry]

def cells( gdc, key=None, sheet=None ):
  "Return the cells from the specified spreadsheet and worksheet."
  if not key:
    raise ValueError( "must supply spreadsheet key" )
  if not sheet:
    raise ValueError( "must supply sheet name" )

  #entry = sheetByName( gdc, key, sheet )
  entry = sheet
  rows = int( entry.row_count.text )
  cols = int( entry.col_count.text )
  wid = entry.id.text.split( '/' )[-1]

  # read cell data
  # Some cells may be empty, and would have no entries without this option.
  query = gdata.spreadsheet.service.CellQuery()
  query.return_empty = "true"
  cells = gdc.GetCellsFeed( key, wksht_id=wid, query=query )

  data = []
  i = 0  
  for r in range( rows ):
    temp = []
    for c in range( cols ):
      temp.append( cells.entry[i].cell.text )
      i += 1
    data.append( temp )

  return data


if __name__ == "__main__":
  if len(sys.argv) < 2:
    print >> sys.stderr, "%s <gdoc_spreadsheet_key>" % (sys.argv[0],)
    sys.exit(1)
  key = sys.argv[1]
  gdc = gdClient()
  sheets = sheets(gdc, sys.argv[1]) 
  for sheet in sheets:
    print "subject: %s" % (sheet.title.text,)
    c = cells(gdc, key, sheet)
    print c

  
