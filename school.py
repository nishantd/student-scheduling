import sys

class Student(object):
  def __init__(self, name, index, required_classes=None):
    """required_classes: [(class_name, num_flexblocks_required, consecutive [true|false]),]
    """
    self.name = name
    self._index = index
    self.required_classes = required_classes
    self.assigned_flexblocks = None  # {'class1':'Flexblock name',..}

  def __str__(self):
    return "name=%s _index=%s required_classes=%s" \
        % (self.name, self._index, ','.join("class: %s.%i.%s" % (c,n,con) for (c,n,con) in self.required_classes))


class Class(object):
  """A class is a unit of teaching material of a Subject.
  I.e. e1.1, m2.1 etc.
  """
  def __init__(self, name, index, overall_class_index, subject_name): #, flexblock_names=None):
    self.name = name
    self._index = index
    self.overall_class_index = overall_class_index
    self.subject_name = subject_name
    # Names of flexblocks in which this class can be scheduled.
    #self.flexblock_names = flexblock_names

  def __str__(self):
    s = "name=%s _index=%s oci=%s subject_name=%s" \
        % (self.name, self._index, self.overall_class_index, self.subject_name)
    #if self.flexblock_names:
    #  s += " flexblock_names=%s" % (flexblock_names,)
    return s


class Subject(object):
  """A subject contains all the flexblock (names) that it can be scheduled in.
  """
  def __init__(self, name, flexblock_names=None):
    """flexblock_names: a list of valid flexblock names
    """
    self.name = name
    self.flexblock_names = flexblock_names
    self.class_names = None  # To be assigned later

  def __str__(self):
    return "name=%s flexblock_names=%s class_names=%s" \
        % (self.name, self.flexblock_names, self.class_names)


class FlexBlock(object):
  def __init__(self, name, index, overall_flexblock_index, timeslot=None):
    self.name = name
    self._index = index
    self.timeslot = timeslot
    self.is_valid = False
    self.day_index = None
    self.overall_flexblock_index = overall_flexblock_index

  def __str__(self):
    return "name=%s overall_flexblock_index=%s _index=%s timeslot=%s is_valid=%s" % \
        (self.name, self.overall_flexblock_index, self._index, self.timeslot, self.is_valid)


class School(object):
  def __init__(self, students_dict, classes_dict, subjects_dict, flexblocks_dict):
    self.students_dict = students_dict
    self.classes_dict = classes_dict
    self.subjects_dict = subjects_dict
    self.flexblocks_dict = flexblocks_dict

