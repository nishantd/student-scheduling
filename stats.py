
import school

def classes_flexblocks(students_dict):
  """returns: {class_name:[num_students, num_flexblocks_required],}
  """
  class_stats = {}
  total_flexblocks_required = 0
  for student in students_dict.values():
    if not student.required_classes: continue
    for (class_name, num_flexblocks_required, consecutive) in student.required_classes:
      class_stat = class_stats.setdefault(class_name, [0,0])  # [num_students, num_flexblocks_required]
      class_stat[0] += 1
      class_stat[1] += num_flexblocks_required
      total_flexblocks_required += num_flexblocks_required
  class_stats['total'] = [None, total_flexblocks_required]
  return class_stats

