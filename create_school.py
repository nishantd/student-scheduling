#!/usr/bin/python

import optparse
import os, sys

#from . import school
import school
import gdoc_util

STUDENT_PROGRESS_KEY = '0AsXt5PuHTFbddEJMQ3MxYUI1cXFUX3o5OUx3aHZpaGc'
FLEXBLOCKS_KEY = '0AsXt5PuHTFbddHlMTTNuSGs2QUJmTVUzSW5JQkZIUHc'

#SUBJECTS = ['English', 'Math', 'Physics', 'Econ']
#SUBJECTS = ['English', 'Math']
SUBJECTS = ['Math']

SUBJECTS_STR = os.getenv('SUBJECTS_STR', None)
if SUBJECTS_STR:
  SUBJECTS = [s.strip().lower() for s in SUBJECTS_STR.strip().split(',')]
print "SUBJECTS: %s" % (SUBJECTS,)

def _canonicalize(x):
  return x.lower()

def get_subjects_flexblocks(gdc, key, flexblock_sheet):
  """Knows how to read the teacher flexblock schedule.
  Returns ({subject_name:Subject,}, {flexblock_name:Flexblock,})
  All names are canonicalized.
  """
  flexblocks_per_day = int(os.getenv('FLEXBLOCKS_PER_DAY', 8))
  c = gdoc_util.cells(gdc, key, flexblock_sheet)
  print >> sys.stderr, "%s" % (c,)
  # Row 1 sets the number of columns to look at.
  num_columns = 0
  for cell in c[0][1:]:
    if cell is None: break
    num_columns += 1
    
  # Now the actual flexblocks and subjects
  subjects = {}
  flexblocks = {}
  overall_flexblock_index = 0
  subject_rows = {}
  for subject_row in c[1:]:
    if subject_row[0] is None: break
    print >> sys.stderr, "subject_row: %s" % (subject_row[0],)
    subject_name = _canonicalize(subject_row[0]).split('_')[0]
    if subject_name not in SUBJECTS: 
      print >> sys.stderr, "%s not in %s" % (subject_name, SUBJECTS)
      continue
    assert not subject_rows.has_key(subject_row[0])
    subject_rows[subject_row[0]] = None
    subject = subjects.get(subject_name)
    if subject is None:
      subject = school.Subject(
        name=subject_name, flexblock_names=[])
      subjects[subject_name] = subject
    index = 0
    for cell in subject_row[1:num_columns+1]:
      print >> sys.stderr, "overall_flexblock_index=%s, index=%s, cell=%s" % \
          (overall_flexblock_index, index, cell)
      flexblock_name = "%s_%s" % (_canonicalize(subject_row[0]), c[0][index+1])
      f = school.FlexBlock(name=flexblock_name, index=index, 
                           overall_flexblock_index=overall_flexblock_index)
      flexblocks[f.name] = f
      f.day_index = index / flexblocks_per_day
      if cell and cell.strip().lower() == 't':
        f.is_valid = True
        subject.flexblock_names.append(f.name)
      index += 1
      overall_flexblock_index += 1

  return (subjects, flexblocks)


def get_classes_students_info(gdc, key, subjects):
  """Knows how to read the student progress tracker.
  Input: 
  subjects: dictionary of subjects with subject_name as key
  flexblocks: dictionary of flexblocks
  Returns {student_name:Student,}
  """
  override_num_blocks = int(os.getenv('OVERRIDE_NUM_BLOCKS',0))
  # bool('0') = True, bool(int('0')) = False
  always_consecutive = bool(int(os.getenv('ALWAYS_CONSECUTIVE','0')))
  classes = {}
  student_classes = set()
  students = {}
  sheets = gdoc_util.sheets(gdc, key)
  sheets_ctr = 0
  def _cmp_sheets(x, y): return cmp(x.title.text, y.title.text)
  sheets.sort(cmp=_cmp_sheets)
  overall_class_index = 0
  for sheet in sheets:
    sheet_name = _canonicalize(sheet.title.text)
    print >> sys.stderr, "sheet name = %s" % (sheet_name,)
    if not subjects.has_key(sheet_name):
      print >> sys.stderr, "sheet %s is not a subject" % (sheet_name,)
      continue
    subject_name = sheet_name
    subject = subjects[subject_name]
    classes_array = []
    cells = gdoc_util.cells(gdc, key, sheet)
    # First row is the classes.
    index = 0
    for cell in cells[0][1:]:
      if not cell: break
      c = school.Class(
          name=_canonicalize(cell), index=index, 
          overall_class_index=overall_class_index, subject_name=subject_name)
      classes[c.name] = c
      classes_array.append(c)
      index += 1
      overall_class_index += 1
    num_classes = index
    subject.class_names = [c.name for c in classes_array]
    subject.class_names.sort()
    # Then come all the students, with student name in the first column
    student_index = 0
    for row in cells[1:]:
      if not row[0] or not row[0].strip():
        break
      student_name = _canonicalize(row[0])
      student = students.get(student_name)
      if sheets_ctr > 0:
        assert student, "could not find student (%s)" % (student.name,)
        assert student._index == student_index, "student index does not match: %s %s" % (student._index, student_index)
      class_index = 0
      required_classes = []
      for col in row[1:]:
        if class_index == num_classes:
          break
        if col and col.strip():
          class_req = col.strip()
          assert len(class_req) >= 1, "bad class_req: %s" % (class_req,)
          consecutive = False
          if len(class_req) == 1:
            num_blocks = 1
          elif len(class_req) == 2:
            num_blocks = int(class_req[1])
          elif len(class_req) == 3:
            num_blocks = int(class_req[1])
            assert class_req[2] == 'c', "unrecognized class specification ()" % (class_req,)
            consecutive = True
          if override_num_blocks:
            num_blocks = override_num_blocks
          if always_consecutive:
            consecutive = True
          required_classes.append((classes_array[class_index].name, num_blocks, consecutive))
          student_classes.add(classes_array[class_index])
        class_index += 1
      if student:
        rc = student.required_classes
        rc.extend(required_classes)
      else:
        student = school.Student(student_name, student_index, required_classes)
        students[student.name] = student
      student_index += 1
    sheets_ctr += 1  
  taken_classes = dict([(c.name, c) for c in student_classes])
  return (taken_classes, students)

def to_str(school_obj, stream):
  print >> stream, "-------------------------"
  print >> stream, "SCHOOL:"
  subjects = school_obj.subjects_dict
  print >> stream,  "subjects: %s\n%s" \
      % (len(subjects), '\n'.join(["%s:%s" % (k,v) for (k,v) in subjects.iteritems()]))
  flexblocks = school_obj.flexblocks_dict
  print >> stream, "flexblocks: %s\n%s" \
      % (len(flexblocks), '\n'.join(["%s:%s" % (k,v) for (k,v) in flexblocks.iteritems()]))
  classes = school_obj.classes_dict
  print >> stream, "classes: %s\n%s" \
      % (len(classes), '\n'.join(["%s:%s" % (k,v) for (k,v) in classes.iteritems()]))
  students = school_obj.students_dict
  print >> stream, "students: %s\n%s" \
      % (len(students), '\n'.join(["%s:%s" % (k,v) for (k,v) in students.iteritems()]))
  print >> stream, "-----------------------------\n"

def create_school():
  gdc = gdoc_util.gdClient()
  sheets = gdoc_util.sheets(gdc, FLEXBLOCKS_KEY) 
  flexblock_sheet = None
  for sheet in sheets:
    if sheet.title.text == 'Flexblocks':
      flexblock_sheet = sheet
      break
  assert flexblock_sheet, "no Flexblocks sheet found!"
  (subjects, flexblocks) = get_subjects_flexblocks(
      gdc, FLEXBLOCKS_KEY, flexblock_sheet)
  (classes, students) = get_classes_students_info(
      gdc, STUDENT_PROGRESS_KEY, subjects)
  def _cmp(c1, c2):
    return cmp(c1.overall_class_index, c2.overall_class_index)
  _tmp = classes.values()
  _tmp.sort(cmp=_cmp)
  #print "|".join("%s" % (e,) for e in _tmp)
  s =  school.School(students, classes, subjects, flexblocks)
  return s

if __name__ == "__main__":
  create_school()

